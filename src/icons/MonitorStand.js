import * as React from 'react'

function SvgMonitorStand(props) {
  return (
    <svg viewBox="0 0 177 49" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path fill="none" d="M-1-1h179v51H-1z" />
      <g>
        <g transform="scale(.82645)">
          <path fill="none" d="M1.21 0h222v67h-222z" />
        </g>
        <g transform="scale(.82645)">
          <g stroke="null">
            <path
              fill="#b3b3b3"
              d="M817106 603004.451h-1763l-96 177h1859z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#a8a8a8"
              d="M817106 603004.451h1763l96 177h-1859z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#dbdbdb"
              d="M815247.063 603181.551h1859v68.66h-1859z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#c5c5c5"
              d="M817106.063 603181.551h1859v68.66h-1859z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#1f1f1f"
              d="M815255 603250.451h1851v89h-1838z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#161616"
              d="M818957 603250.451h-1851v89h1838z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#383737"
              d="M816793.063 603061.551h313v120h-313z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#2b2b2b"
              d="M817106.063 603061.551h313v120h-313z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#1c1c1c"
              d="M816793.063 602219.551h313v842h-313z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="#090909"
              d="M817106.063 602219.551h313v842h-313z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
            <path
              fill="none"
              d="M816793.063 602275.551h627v197h-627z"
              transform="matrix(-.05758 0 0 .05392 47154.435 -32473.384)"
            />
          </g>
        </g>
      </g>
    </svg>
  )
}

export default SvgMonitorStand
