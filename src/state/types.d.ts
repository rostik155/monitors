import { TMonitors } from './monitors/types'

export interface IAppState {
  monitors: TMonitors
}
