import { combineReducers, Reducer } from 'redux'
import monitors from './monitors/reducer'

const rootReducer: Reducer = combineReducers({
  monitors,
})

export default rootReducer
