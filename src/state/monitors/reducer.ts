import { cloneDeep, findKey } from 'lodash'
import { IAppState } from '../types'
import { IMonitor, TMonitors } from './types'
import localStorageReducerDecorator from '../../utils/localStorageReducerDecorator'

// Action types
const NS = 'monitors'
export const CHANGE = `${NS}/CHANGE`
export const ADD = `${NS}/ADD`
export const REMOVE = `${NS}/REMOVE`

const getInitialState = (): TMonitors => [
  {
    id: 0,
    width: 1920,
    height: 1080,
  },
  {
    id: 1,
    width: 2560,
    height: 1440,
  },
  {
    id: 2,
    width: 1024,
    height: 768,
  },
]

const monitors = (state: TMonitors = getInitialState(), action: any) => {
  switch (action.type) {
    case CHANGE: {
      const newState: TMonitors = cloneDeep(state)
      const key = findKey(newState, { id: action.id })
      if (key !== undefined) {
        newState[+key] = { ...newState[+key], ...action.payload }
      }
      return newState
    }
    case ADD: {
      const newState: TMonitors = cloneDeep(state)
      const allIds = newState.map((monitor) => monitor.id)
      const id = allIds.length === 0 ? 0 : Math.max(...allIds) + 1
      newState.push({ ...action.payload, id })
      return newState
    }
    case REMOVE: {
      const newState: TMonitors = cloneDeep(state)
      const key = findKey(newState, { id: action.payload })
      if (key !== undefined) {
        newState.splice(+key, 1)
      }
      return newState
    }
    default:
      return state
  }
}

export default localStorageReducerDecorator({
  reducer: monitors,
  key: 'monitors',
  defaultState: getInitialState(),
})

// Actions
export const updateAction = (id: number, properties: Partial<IMonitor>) => {
  return { id, payload: properties, type: CHANGE }
}

export const addAction = (properties: Partial<IMonitor>) => {
  return { payload: properties, type: ADD }
}

export const removeAction = (id: number) => {
  return { payload: id, type: REMOVE }
}

// Selectors
export const getMonitors = (state: IAppState): TMonitors => state.monitors
