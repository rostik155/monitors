export interface IMonitor {
  id: number,
  width: number,
  height: number,
}

export type TMonitors = Array<IMonitor>
