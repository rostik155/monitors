import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMonitors, addAction } from '../state/monitors/reducer'
import { TMonitors } from '../state/monitors/types'
import Monitor from './Monitor'

const calculateRatio = (monitors: TMonitors): number => {
  const maxWidthInRem: number = 20
  const maxHeightInRem: number = 11.25
  return monitors.reduce((maxRatio, monitor) => {
    const { width, height } = monitor
    let max: number
    let current: number
    if (width / maxWidthInRem > height / maxHeightInRem) {
      max = maxWidthInRem
      current = width
    } else {
      max = maxHeightInRem
      current = height
    }
    const ratio = current / max
    return ratio > maxRatio ? ratio : maxRatio
  }, 0)
}

const Monitors: React.FC = () => {
  const dispatch = useDispatch()
  const monitors: TMonitors = useSelector(getMonitors)
  const ratio: number = calculateRatio(monitors)

  function addMonitor(): void {
    dispatch(addAction({ width: 1920, height: 1080 }))
  }

  return (
    <div className="monitors">
      <div className="row">
        {monitors.map((monitor) => (
          <Monitor monitor={monitor} ratio={ratio} key={monitor.id} />
        ))}
        <div
          key="add"
          className="col s4"
          onClick={addMonitor}
          onKeyPress={addMonitor}
          role="button"
          tabIndex={0}
        >
          <div className="card grey lighten-3 add-monitor">
            <div className="plus-sign">+</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Monitors
