import React from 'react'
import Monitors from './Monitors'
import './styles/app.scss'

const App: React.FC = () => {
  return (
    <div className="container">
      <h1 className="pink-text text-darken-2">
        Monitors comparison tool
        <span className="badge pink darken-2 white-text">beta</span>
      </h1>
      <Monitors />
    </div>
  )
}

export default App
