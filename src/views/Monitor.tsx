import React, { CSSProperties } from 'react'
import { useDispatch } from 'react-redux'
import { IMonitor } from '../state/monitors/types'
import { MonitorStand } from '../icons'
import './styles/monitor.scss'
import { updateAction, removeAction } from '../state/monitors/reducer'

interface IMonitorProps {
  monitor: IMonitor
  ratio: number
}

const Monitor: React.FC<IMonitorProps> = (props: IMonitorProps) => {
  const dispatch = useDispatch()

  const { monitor, ratio } = props
  const { width, height, id } = monitor
  const style: CSSProperties = {
    width: `${width / ratio}vw`,
    height: `${height / ratio}vw`,
  }

  function onChange(
    property: string
  ): (e: React.ChangeEvent<HTMLInputElement>) => void {
    return (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { value } = e.target
      dispatch(updateAction(id, { [property]: value === '' ? null : +value }))
    }
  }

  function onRemove(): void {
    dispatch(removeAction(id))
  }

  return (
    <div className="col s4">
      <div className="card red lighten-5">
        <span className="badge teal lighten-1 white-text">
          <input
            type="number"
            value={width == null ? '' : width}
            className="browser-default"
            onChange={onChange('width')}
          />
          x
          <input
            type="number"
            value={height == null ? '' : height}
            className="browser-default"
            onChange={onChange('height')}
          />
        </span>
        <i
          className="material-icons delete-icon"
          role="button"
          tabIndex={0}
          onClick={onRemove}
          onKeyPress={onRemove}
        >
          delete
        </i>
        <div className="center">
          <div className="monitor grey darken-3" style={style} />
          {MonitorStand({
            width: '100px',
          })}
        </div>
      </div>
    </div>
  )
}
export default Monitor
