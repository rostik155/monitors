import { Action, Reducer } from 'redux'

const NS: string = 'state'

interface IArgumentsData<S, A extends Action> {
  reducer: Reducer<S, A>
  key: string
  version?: string
  defaultState: S
}

export default function localStorageReducerDecorator<S, A extends Action>(
  data: IArgumentsData<S, A>
): Reducer<S, A> {
  const { reducer, key, version, defaultState } = data

  let fullKey: string = `${NS}.${key}`
  if (version != null) {
    fullKey = `${fullKey}.${version}`
  }

  const initialState: S = Array.isArray(defaultState)
    ? JSON.parse(localStorage.getItem(fullKey) || 'null') || defaultState || []
    : {
        ...(defaultState || {}),
        ...JSON.parse(localStorage.getItem(fullKey) || '{}'),
      }

  return (state: S = initialState, action: A): S => {
    const newState = reducer(state, action)
    localStorage.setItem(fullKey, JSON.stringify(newState))
    return newState
  }
}
