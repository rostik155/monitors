import { createStore, Store } from 'redux'
import { devToolsEnhancer } from 'redux-devtools-extension'
import rootReducer from './state'

const store: Store = createStore(rootReducer, devToolsEnhancer({}))

export default store
